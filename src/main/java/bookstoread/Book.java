package bookstoread;

import java.time.LocalDate;

public class Book implements Comparable<Book> {

    private final String title;
    private final String author;
    private final LocalDate publishedOn;
    LocalDate startedReadingOn;
    LocalDate finishedReadingOn;

    public Book(String title, String author, LocalDate publishedOn) {
        this.title = title;
        this.author = author;
        this.publishedOn = publishedOn;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public LocalDate getPublishedOn() {
        return publishedOn;
    }

    @Override
    public String toString() {
        return "Book{" + "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", publishedOn=" + publishedOn + '}';
    }

    @Override
    public int compareTo(Book o) {
        return this.getTitle().compareTo(o.getTitle());
    }

    public void startedReadingOn(LocalDate startedReadingOn) {
        this.startedReadingOn = startedReadingOn;
    }

    public void finishedReadingOn(LocalDate finishedReadingOn) {
        this.finishedReadingOn = finishedReadingOn;
    }

    public boolean isRead() { return startedReadingOn != null && finishedReadingOn != null; }

    // TODO: add isProgress() to the Book class and use it to filter the stream of Books.
    public boolean isProgress() { return startedReadingOn != null && finishedReadingOn == null; }
}